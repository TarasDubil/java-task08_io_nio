package com.dubilok.service;

import com.dubilok.model.task_2.Droid;
import com.dubilok.model.task_2.Ship;
import com.dubilok.model.task_3.Task_3;
import com.dubilok.model.task_5.Task_5;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class ManagerImpl implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerImpl.class);

    public void showTask2() {
        serializedShip();
        deserializedShip();
    }

    public void showTask3() {
        Task_3 task = new Task_3();
        task.getMapReading().forEach((key, value) -> logger.info("Buffer size = " + key + "; Time = " + value));
        task.getMapWriting().forEach((key, value) -> logger.info("Buffer size = " + key + "; Time = " + value));
    }

    @Override
    public void showTask4() {

    }

    @Override
    public void showTask5() {
        Task_5 task5 = new Task_5();
        logger.info("All comments");
        task5.getComments().forEach(logger::info);
    }

    @Override
    public void showTask6() {

    }

    @Override
    public void showTask7() {

    }

    @Override
    public void showTask8() {

    }

    private void serializedShip() {
        List<Droid> list = Arrays.asList(new Droid("M5", 500, "Lviv")
                , new Droid("M6", 600, "Lviv"));
        Ship ship = new Ship("Ship", 1000, "Lviv", list);
        logger.info(ship);
        try(FileOutputStream fileOutputStream = new FileOutputStream("ship.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
            objectOutputStream.writeObject(ship);
            logger.info("Serialized data is saved to ship.txt");
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private void deserializedShip() {
        try(FileInputStream fileInputStream = new FileInputStream("ship.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            Ship ship = (Ship) objectInputStream.readObject();
            logger.info(ship);
            logger.info("Deserialized data is saved to ship ");
        } catch (IOException e) {
            logger.error(e);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        }
    }

}
