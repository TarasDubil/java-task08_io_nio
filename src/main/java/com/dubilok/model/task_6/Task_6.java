package com.dubilok.model.task_6;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class Task_6 {

    public static void catalogList(File[] files, String symbol, List<String> list) {
        for (File file : files) {
            list.add(symbol + "↳" + file.getName());
            if (file.isDirectory()) {
                File[] insideFiles = file.listFiles();
                if (insideFiles != null) {
                    catalogList(insideFiles, symbol + "  ", list);
                }
            }
        }
    }

    private void showFile(String path) {
        List<String> list = new LinkedList<>();
        File[] file = {new File(path)};
        catalogList(file, "", list);
        list.forEach(System.out::println);
    }

    public static void main(String[] args) {
        new Task_6().showFile("src");
    }
}
