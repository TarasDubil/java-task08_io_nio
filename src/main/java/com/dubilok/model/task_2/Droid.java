package com.dubilok.model.task_2;

import java.io.Serializable;

public class Droid implements Serializable{
    private String model;
    private double weight;
    private transient String flyTo;

    public Droid(String model, double weight, String flyTo) {
        this.model = model;
        this.weight = weight;
        this.flyTo = flyTo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "model='" + model + '\'' +
                ", weight=" + weight +
                ", flyTo='" + flyTo + '\'' +
                '}';
    }
}
