package com.dubilok.model.task_2;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {
    private String name;
    private int weight;
    private transient String flyTo;
    private List<Droid> droids;

    public Ship(String name, int weight, String flyTo, List<Droid> droids) {
        this.name = name;
        this.weight = weight;
        this.flyTo = flyTo;
        this.droids = droids;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", flyTo='" + flyTo + '\'' +
                ", droids=" + droids +
                '}';
    }
}
