package com.dubilok.model.task_3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Task_3 {

    private static Logger logger = LogManager.getLogger(Task_3.class);

    public Task_3() {
        start("C:\\Users\\Z30\\Videos\\v.flv", "C:\\Users\\Z30\\Videos\\v.flv");
    }

    private Map<Integer, Long> mapReading = new TreeMap<>();
    private Map<Integer, Long> mapWriting = new TreeMap<>();
    private List<Byte> data = new ArrayList<>();

    private void getDataFromFileWitFIS(File file) {
        long start = System.currentTimeMillis();
        try(FileInputStream fileInputStream = new FileInputStream(file)) {
            while (fileInputStream.available() > 0) {
                data.add((byte) fileInputStream.read());
            }
        } catch (IOException e) {
            logger.error(e);
        }
        long finish = System.currentTimeMillis();
        mapReading.put(1, finish - start);
    }

    private void getDataFromFileWithBuffer(File file, int bufferSize) {
        long start = System.currentTimeMillis();
        try(BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file)), bufferSize)) {
            while (bufferedReader.ready()) {
                bufferedReader.read();
            }
        } catch (IOException e) {
            logger.error(e);
        }
        long finish = System.currentTimeMillis();
        mapReading.put(bufferSize, finish - start);
    }

    private void writeDataToFileFOS(File file) {
        long start = System.currentTimeMillis();
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            for (Byte b : data) {
                fileOutputStream.write(b);
            }
        } catch (IOException e) {
            logger.error(e);
        }
        long finish = System.currentTimeMillis();
        mapWriting.put(1, finish);
    }

    private void writeDataToFileWithBuffer(File file, int bufferSize) {
        long start = System.currentTimeMillis();
        try(BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(file)), bufferSize)) {
            for (Byte b : data) {
                bufferedWriter.write(b);
            }
        } catch (IOException e) {
            logger.error(e);
        }
        long finish = System.currentTimeMillis();
        mapWriting.put(bufferSize, finish);
    }

    public void start(String fileNameReading, String fileNameWriting) {
        logger.info("Reading:");
        reading(fileNameReading);
        System.out.println();
        logger.info("Writing: ");
        writing(fileNameWriting);
    }

    private void reading(String fileNameReading) {
        logger.info("Reading data from file: ");
        getDataFromFileWitFIS(new File(fileNameReading));
        for (int i = 1; i <= 10; i++) {
            getDataFromFileWithBuffer(new File(fileNameReading), 512 * i);
        }
    }

    private void writing(String fileNameWriting) {
        logger.info("Writing data to file: ");
        writeDataToFileFOS(new File(fileNameWriting));
        for (int i = 1; i < 10; i++) {
            getDataFromFileWithBuffer(new File(fileNameWriting), 512 * i);
        }
    }

    public Map<Integer, Long> getMapReading() {
        return mapReading;
    }

    public Map<Integer, Long> getMapWriting() {
        return mapWriting;
    }
}
