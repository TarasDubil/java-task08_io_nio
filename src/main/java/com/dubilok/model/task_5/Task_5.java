package com.dubilok.model.task_5;

import com.dubilok.model.task_3.Task_3;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task_5 {
    private static Logger logger = LogManager.getLogger(Task_3.class);
    private List<String> listData;
    private List<String> comments;

    public Task_5() {
        listData = getDataFromFile("task_5.txt");
        comments = getCommentsFromFile();
    }

    private List<String> getDataFromFile(String name) {
        List<String> data = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(new File(name)))) {
            while (reader.ready()) {
                data.add(reader.readLine().trim());
            }
        } catch (IOException e) {
            logger.error(e);
        }
        return data;
    }

    private List<String> getCommentsFromFile() {
        List<String> comments = listData.stream()
                .filter(e -> e.startsWith("/") || e.startsWith("*"))
                .collect(Collectors.toList());
        return comments;
    }

    public List<String> getComments() {
        return comments;
    }
}
