package com.dubilok.view;

import com.dubilok.controller.Controller;
import com.dubilok.controller.ControllerImpl;
import com.dubilok.service.Manager;
import com.dubilok.service.ManagerImpl;
import com.dubilok.util.UtilMenu;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ViewImpl implements View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Manager manager;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ViewImpl() {
        controller = new ControllerImpl();
        manager = new ManagerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Create Ship with Droids. Serialize and deserialize them. Use transient.");
        menu.put("2", "  2 - Start problem Minesweeper.");
        menu.put("3", "  3 - Compare reading and writing performance.");
        menu.put("4", "  4 - Display all comments from file 'task_5.txt'.");
        menu.put("5", "  5 - ");
        menu.put("6", "  6 - Start problem Minesweeper.");
        menu.put("7", "  7 - Start problem Minesweeper.");
        menu.put("Q", "  Q - exit.");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton2);
        methodsMenu.put("2", this::pressButton3);
        methodsMenu.put("3", this::pressButton4);
        methodsMenu.put("4", this::pressButton5);
        methodsMenu.put("5", this::pressButton6);
        methodsMenu.put("6", this::pressButton7);
        methodsMenu.put("7", this::pressButton8);
    }

    private void pressButton2() {
        manager.showTask2();
    }

    private void pressButton3() {
        manager.showTask3();
    }

    private void pressButton4() {
        manager.showTask4();
    }

    private void pressButton5() {
        manager.showTask5();
    }

    private void pressButton6() {
        manager.showTask6();
    }

    private void pressButton7() {
        manager.showTask7();
    }

    private void pressButton8() {
        manager.showTask8();
    }

    @Override
    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
